/*
 * Copyright (c) 2007-2014, Anthony Minessale II
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * * Neither the name of the original author; nor the names of any contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * The Initial Developer of the Original Code is
 * Anthony Minessale II <anthm@freeswitch.org>
 * Portions created by the Initial Developer are Copyright (C)
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 * Anthony Minessale II <anthm@freeswitch.org>
 * Michael B. Murdock <mike@mmurdock.org>
 * António Silva <asilva@wirelessmundi.com>
 *
 * mod_counter_load.c -- Telnyx counter load module
 * Author : Rodrigo C. G. França
 *
 */

#include <switch.h>
#include <math.h>
#include <ctype.h>

SWITCH_MODULE_LOAD_FUNCTION(mod_counter_load);                                                                                                                                                  SWITCH_MODULE_LOAD_FUNCTION(mod_counter_load);
SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_counter_shutdown);
SWITCH_MODULE_DEFINITION(mod_counter_load, mod_counter_load, mod_counter_shutdown, NULL);

static int counter = 0;

SWITCH_STANDARD_API(display_counter)
{
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "******************************************\r\n");
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "[display_counter] counter = %d \r\n", counter);
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "******************************************\r\n");

    return SWITCH_STATUS_SUCCESS;
}

static void all_events_handler(switch_event_t *event)
{
    char *p_Dest = NULL;
    char *p_Telnyx = NULL;
    char *p_CreateChannel = NULL;
    char *buf;
    switch_event_serialize(event, &buf, SWITCH_TRUE);
    //switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_CONSOLE,"\nEVENT ------------------------------\n%s",buf);

    p_Dest = strstr(strdup(buf), "variable_sip_destination_url");
    p_Telnyx = strstr(strdup(buf), "sip.telnyx.com");
    p_CreateChannel = strstr(strdup(buf), "CHANNEL_CREATE");

    if (p_Dest && p_Telnyx && p_CreateChannel)
    {
        counter++;

        switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "******************************************\r\n");
        switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "==> CALL TO sip.telnyx.com\n");
        switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_INFO, "******************************************\r\n");
    }
}

SWITCH_MODULE_LOAD_FUNCTION(mod_counter_load)
{
    switch_api_interface_t *api_interface;
    *module_interface = switch_loadable_module_create_module_interface(pool, modname);

    api_interface = switch_loadable_module_create_interface(*module_interface, SWITCH_API_INTERFACE);
    api_interface->interface_name = "display_counter";

    SWITCH_ADD_API(api_interface, "display_counter", " Count the number of outbound calls routed towards sip.telnyx.com", display_counter, "display_counter");

    switch_event_bind(modname, SWITCH_EVENT_ALL, SWITCH_EVENT_SUBCLASS_ANY, all_events_handler, NULL);
    return SWITCH_STATUS_SUCCESS;
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_counter_shutdown)
{
    return SWITCH_STATUS_UNLOAD;
}
